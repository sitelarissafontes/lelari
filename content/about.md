---
title: "About"
date: 2022-10-05T15:03:32-03:00
draft: false
---

Larissa Fontes

Moradora de São Paulo, estudante do 6º semestre de Engenharia Mecatrônica na Poli USP e no auge dos seus 21 anos, Larissa apresenta uma segunda paixão além de robótica, que são os Livros! 

Filha de dois professores, desde pequena foi muito incetivada a incluir leituras na sua rotina, tornando-se assim uma amante de todos os mais variados gêneros literários, como: ficcção, fantasia épica, literatura noir, brasileira, romances e entre outros. 

Criado em Outubro de 2022, o Lê Lari também é um fruto dessa paixão. Tendo como principal intuito compartilhar os livros mais tocantes e interessantes lidos em um pouco menos que 21 anos de vida.  

