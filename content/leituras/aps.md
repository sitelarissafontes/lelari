---
title: "A Paciente Silenciosa"
date: 2022-10-09T09:47:19-03:00
draft: false
---

A paciente silenciosa, lançado em 2019, é um livro de Alex Michaelides, que conta um verdadeiro Thriller que não te faz querer parar de ler do começo ao fim. 

O suspense psicológico conta a história de Alice Berenson, uma artista, que após a morte do seu marido, nunca mais consegue dizer nada. 

Após anos de tentativas sem sucesso, Theo Faber, psicoterapeuta criminal, se vê instigado em ser o homem que conseguirá fazer Alice dizer a verdade sobre a morte de seu marido. 

A história escrita por Alex vai misturando o passado com o presente, de tal forma que seja possível ligar ambos momentos, como em um quebrar cabeça. 

Um livro que realmente é capaz de te fazer perder o sono. A cada página, o anseio de saber o que realmente aconteceu no dia que Gabriel, marido de Alice morreu, é revelado de maneira instigante para o leitor. 