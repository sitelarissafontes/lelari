---
title: "As Mentiras de Locke Lamora"
date: 2022-10-09T09:46:39-03:00
draft: false
---

As Mentiras de Locke Lamora

O livro "As mentiras de Locke Lamora", escrito por Scott Lynch com ano de lançamento em 2006, é considerado um grande livro do gênero de fantasia. 

Ambientada em um local baseado na Veneza Medieval: Cammor, uma cidade que concentra uma grande desigualdade social, é o grande background para os feitos de Locke. 

Órfão desde pequeno, Locke é adotado por Correntes, um velho golpista, que se passa por cego para conseguir dinheiro das pessoas. Crescendo desde pequeno  em um mundo de golpes, Locke e seus irmãos adotivos (Jean, Galdo e Calo Sanza e Pulga) formam o grupo conhecido por Cammor como os "Nobres Vigaristas". 

Temidos por muitos, mas pouco conhecida suas identidades, Locke e os seus irmãos aplicam diversos golpes com diferentes disfarces em toda nobreza de Cammor, ao longo do livro. Entretanto, Locke aprende que há pessoas com quem se mexe, que o preço é alto demais a se pagar. 

O livro traz uma grande mistura de fantasia embalada em uma vingança extremamente apaixonante. Uma vingança motivada pelo amor. 

Para mim, enquanto leitora, o primeiro livro da trilogia dos Nobres Vigaristas, é uma história que acho que todos deveriam ler. E tenho certeza que quando você terminar, irá sair correndo para comprar o Vol.2. 


